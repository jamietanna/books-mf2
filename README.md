# books-mf2

A translation layer for books that converts them to the [Microformats2](https://microformats.io) JSON format.
