package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/jamietanna/books-mf2/internal/openlibrary"
	"willnorris.com/go/microformats"
)

type BookResponse struct {
	Items []microformats.Microformat `json:"items"`
}

func handleIsbn(w http.ResponseWriter, r *http.Request) {
	isbn := chi.URLParam(r, "isbn")
	if isbn == "" {
		http.Error(w, "Invalid ISBN", http.StatusBadRequest)
		return
	}

	mf2, err := openlibrary.Retrieve(isbn)
	if err != nil {
		log.Printf("Failed to retrieve isbn=%s from OpenLibrary: %v", isbn, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var resp BookResponse
	resp.Items = []microformats.Microformat{mf2}

	w.Header().Add("content-type", "application/mf2+json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("welcome"))
	})
	r.Get("/isbn/{isbn:[0-9]{10,13}}", handleIsbn)

	http.ListenAndServe(":"+port, r)
}
