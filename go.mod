module gitlab.com/jamietanna/books-mf2

go 1.19

require (
	github.com/go-chi/chi/v5 v5.0.8
	willnorris.com/go/microformats v1.2.0
)

require golang.org/x/net v0.2.0 // indirect
