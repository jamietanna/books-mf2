package openlibrary

type Author struct {
	Name  string `json:"name"`
	Links []struct {
		URL string `json:"url"`
	} `json:"links"`
	Photos []int `json:"photos"`
}
