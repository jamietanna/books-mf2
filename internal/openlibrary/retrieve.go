package openlibrary

import (
	"encoding/json"
	"fmt"
	"net/http"

	"willnorris.com/go/microformats"
)

func Retrieve(isbn string) (mf2 microformats.Microformat, err error) {
	mf2.Type = []string{"h-book"}
	mf2.Properties = make(map[string][]any)

	book, err := retrieveBook(isbn)
	if err != nil {
		return
	}

	mf2.Properties["url"] = []any{
		fmt.Sprintf("https://books-mf2.fly.dev/isbn/%s", isbn),
		fmt.Sprintf("https://openlibrary.org%s", book.Key),
	}
	mf2.Properties["uid"] = property(fmt.Sprintf("isbn:%s", book.Isbn13[0]))

	title := book.FullTitle
	if title == "" {
		title = book.Title
	}

	mf2.Properties["name"] = property(title)

	works, foundWorks, err := retrieveWorks(book)
	if err != nil {
		return
	}

	var authorKeys []string
	if foundWorks {
		for _, author := range works.Authors {
			authorKeys = append(authorKeys, author.Author.Key)
		}
	} else {
		for _, author := range book.Authors {
			authorKeys = append(authorKeys, author.Key)
		}
	}

	for _, key := range authorKeys {
		author, err := retrieveAuthor(key)
		if err != nil {
			return mf2, err
		}
		mf2.Properties["author"] = append(mf2.Properties["author"], author)
	}

	if book.Covers != nil {
		mf2.Properties["photo"] = property(
			map[string]string{
				"alt":   fmt.Sprintf("Cover picture of %s", title),
				"value": fmt.Sprintf("https://covers.openlibrary.org/b/id/%d.jpg", book.Covers[0]),
			},
		)
	}

	return
}

func retrieveBook(isbn string) (book Book, err error) {
	resp, err := http.Get(fmt.Sprintf("https://openlibrary.org/isbn/%s.json", isbn))
	if err != nil {
		return
	}

	if resp.StatusCode != 200 {
		return book, fmt.Errorf("received HTTP %d from Open Library ISBN=%s call", resp.StatusCode, isbn)
	}

	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&book)

	return
}

func retrieveWorks(book Book) (works Works, found bool, err error) {
	if book.Works == nil {
		return
	}

	worksPath := book.Works[0].Key

	resp, err := http.Get(fmt.Sprintf("https://openlibrary.org%s.json", worksPath))
	if err != nil {
		return
	}

	if resp.StatusCode != 200 {
		return works, found, fmt.Errorf("received HTTP %d from Open Library works(%s) call", resp.StatusCode, worksPath)
	}

	found = true

	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&works)

	return
}

func retrieveAuthor(key string) (mf2 microformats.Microformat, err error) {
	resp, err := http.Get(fmt.Sprintf("https://openlibrary.org/%s.json", key))
	if err != nil {
		return
	}

	if resp.StatusCode != 200 {
		return mf2, fmt.Errorf("received HTTP %d from Open Library Author(%s) call", resp.StatusCode, key)
	}

	var author Author
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&author)

	mf2.Type = []string{"h-card"}
	mf2.Properties = make(map[string][]any)
	mf2.Properties["name"] = property(author.Name)

	if author.Links != nil {
		mf2.Properties["url"] = property(author.Links[0].URL)
	} else {
		mf2.Properties["url"] = property(fmt.Sprintf("https://openlibrary.org%s", key))
	}

	if author.Photos != nil {
		mf2.Properties["photo"] = property(
			map[string]string{
				"alt":   fmt.Sprintf("Picture of %s", author.Name),
				"value": fmt.Sprintf("https://covers.openlibrary.org/b/id/%d.jpg", author.Photos[0]),
			},
		)
	}

	return
}

func property[T any](s T) []any {
	return []any{s}
}
