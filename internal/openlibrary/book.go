package openlibrary

type Book struct {
	Key       string   `json:"key"`
	Isbn13    []string `json:"isbn_13"`
	Title     string   `json:"title"`
	FullTitle string   `json:"full_title"`

	Authors []struct {
		Key string `json:"key"`
	} `json:"authors"`
	Works []struct {
		Key string `json:"key"`
	} `json:"works"`
	Covers []int `json:"covers"`
}
